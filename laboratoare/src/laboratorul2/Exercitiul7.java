package laboratorul2;

import java.util.Scanner;

public class Exercitiul7 {

    public static void guessTheNumber() {
        int max = 10;
        int min = 1;

        int x = (int) (Math.random() * ((max - min) + 1) + min);
        //to check if the method works as expected
        System.out.println("Number is " + x);

        boolean win = false;
        int attempt = 1;

        while (win == false && attempt < 4) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Write a number between 1 and 10");

            int userNumber = scanner.nextInt();

            if (userNumber == x) {
                win = true;
                System.out.println("Congratulations!");
            } else if (userNumber < x) {
                attempt++;
                System.out.println("Your number is too small.");
            } else if (userNumber > x) {
                System.out.println("Your number is too big.");
                attempt++;
            }
        }
    }

    public static void main(String[] args) {
        guessTheNumber();
    }
}
