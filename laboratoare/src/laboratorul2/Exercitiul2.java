package laboratorul2;

public class Exercitiul2 {

    public void ifPrintNumberInWord(int number){
        if(number == 1){
            System.out.println("ONE");
        }else if(number == 2){
            System.out.println("TWO");
        }else if(number == 3){
            System.out.println("THREE");
        }else{
            System.out.println("NUMBER OUT OF RANGE");
        }
    }

    public void switchPrintNumberInWord(int number){
        switch (number){
            case 1:
                System.out.println("ONE");
                break;
            case 2:
                System.out.println("TWO");
                break;
            case 3:
                System.out.println("THREE");
                break;
            default:
                System.out.println("NUMBER OUT OF RANGE");
                break;
        }
    }

}
